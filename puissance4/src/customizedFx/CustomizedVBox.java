package customizedFx;

import javafx.scene.layout.VBox;

public class CustomizedVBox extends VBox {
	private int num;
	
	public CustomizedVBox (int num) {
		super();
		this.num = num;
	}
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
}

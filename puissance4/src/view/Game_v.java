package view;

import controller.*;
import customizedFx.CustomizedVBox;
import general.Main;
import model.*;

import java.util.ArrayList;
import observer.Observer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Game_v extends Stage implements Observer {
	private Game_c controler;
	private ArrayList<CustomizedVBox> columns = new ArrayList<CustomizedVBox>();
	private int nbRow;
	private int nbColumn;

	private final int tokenSize					= 30;
	private final int spaceBetweenTokens		= 10;
	private final int spaceCorner				= 10;
	private final int sideBarWidth				= 150;
	private int nbToken							= 0;
	private final int gridY						= 2*(tokenSize + spaceCorner);
	private final int playerLogoSize			= 40;
	private final Color[] colorTable			= { Color.ORANGE,
													Color.MEDIUMSLATEBLUE,
													Color.LAWNGREEN,
													Color.DARKRED,
													Color.WHITE				};
    

	private VBox sideBar 						= new VBox();
	private Label labelNbTour 					= new Label("Tour : 0");
	private Button leaveButton					= new Button("Quitter");
	private HBox tophbox = new HBox();
	
	private ArrayList<Player_m> playerList;
	
	public Game_v(Game_c controler){ 
		this.controler=controler;
		this.playerList = controler.getModelGame().getPlayerList();
		this.nbRow = controler.getModelGame().getNbLine();
		this.nbColumn = controler.getModelGame().getNbCol();
		
		this.setTitle("Puissance 4");
		//taille de fenetre fixe (taille de la grille)
		this.setResizable(false);
		this.sizeToScene();
		
		Scene scene = new Scene(creerContenu()); 
		scene.getStylesheets().add("view/style.css");
		this.setScene(scene);
	}
	
	

	
	
	
	private HBox creerContenu() {
		HBox window = new HBox();
		
		/***************************************************************************************************
		 * Side zone
		 */
	
		sideBar.setPrefWidth(sideBarWidth);
		sideBar.setMaxWidth(sideBarWidth);
		sideBar.setSpacing(spaceCorner);
		sideBar.setAlignment(Pos.CENTER);
		
		int indexfirstPlayer=this.controler.getModelGame().getPlayerList().indexOf(this.controler.getModelGame().getCurrentPlayer());
		for (int player = 0 ; player < this.playerList.size() ; player++) {
				HBox playerBox = new HBox();
			playerBox.setAlignment(Pos.CENTER_LEFT);
			
			Rectangle logo = new Rectangle(playerLogoSize,playerLogoSize);
			logo.setFill(colorTable[player]);
			Label labelPseudo = new Label(this.playerList.get((indexfirstPlayer+player)%this.playerList.size()).getPseudo());
			
			playerBox.getChildren().addAll(logo,labelPseudo);
			sideBar.getChildren().add(playerBox);
		}
		leaveButton.setOnAction(e->{close();});
		leaveButton.getStyleClass().add("btn-rouge");
		sideBar.getChildren().addAll(labelNbTour, leaveButton);
		
		
		
		/***************************************************************************************************
		 * Main zone
		 */
		VBox mainzone = new VBox();
		
	
		tophbox.setPadding(new Insets(spaceCorner));
		tophbox.setSpacing(spaceBetweenTokens);
		HBox gridhbox = new HBox();
		gridhbox.setStyle("-fx-background-color:CORNFLOWERBLUE");
		gridhbox.setPadding(new Insets(0,spaceBetweenTokens/2,0,spaceBetweenTokens/2));
	
		for (int iColumn = 0 ; iColumn < nbColumn ; iColumn++) {
			CustomizedVBox column = new CustomizedVBox(iColumn);
			this.columns.add(column);
			tophbox.getChildren().add(new Circle(tokenSize, colorTable[0]));
			tophbox.getChildren().get(column.getNum()).setVisible(false);
			column.setSpacing(spaceBetweenTokens);
			column.setPadding(new Insets(spaceCorner,spaceBetweenTokens/2,spaceCorner,spaceBetweenTokens/2));
			column.hoverProperty().addListener(new ChangeListener<Boolean>() {
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
	                if (newValue) {
	                	tophbox.getChildren().get(column.getNum()).setVisible(true);
	                } else {
	                	tophbox.getChildren().get(column.getNum()).setVisible(false);
	                }
				}
	        });
			column.setOnMousePressed(e->{
				this.controler.drop(column.getNum());
			});
			for (int iRow = 0 ; iRow < nbRow ; iRow++) {
				column.getChildren().add(new Circle(tokenSize, colorTable[4]));
			}
			gridhbox.getChildren().add(column);
		}
		HBox.setMargin(gridhbox, new Insets(gridY,0,0,0));

		
		mainzone.getChildren().addAll(tophbox,gridhbox);
		window.getChildren().addAll(sideBar,mainzone);
		
		return window;
	}
	public void update(Game_m game) {
		int col=game.getLastToken().getPosition().getCol();
		int line=game.getLastToken().getPosition().getLine();
		int id_color=game.getPlayerList().indexOf(game.getCurrentPlayer());
		labelNbTour.setText("Tour : "+game.getNbTours());
		Player_m  winner=game.getWinner();
		
		
		if(winner!=null) {
			
			Main.openWinnerModal(this,winner);
		}else {
			
		}
		this.changeColor(line, col, id_color,game.getNbCol(),game.getPlayerList().size());
	}
	
	/**
	 * 
	 * @Brief change color token
	 * @param line
	 * @param col
	 */
	private void changeColor(int line,int col,int id,int nbCol,int nbPlayer) {

		Circle token,tokentop;
		token=(Circle) this.columns.get(col).getChildren().get(line);
		token.setFill(this.colorTable[this.nbToken%nbPlayer]);
	
		for(int i=0;i<nbCol;i++) {
			tokentop=(Circle) tophbox.getChildren().get(i);
			tokentop.setFill(this.colorTable[(this.nbToken+1)%nbPlayer]);
		}
		this.nbToken++;
	}
}

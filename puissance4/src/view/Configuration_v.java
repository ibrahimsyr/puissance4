package view;
import controller.Conf_c;

import java.util.ArrayList;

import general.Main;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Player_m;

public class Configuration_v extends Stage {
	private Conf_c controller;
	
	//player option components
	static private ListView<String> playerView 	= new ListView<String>();
	static private Label addPlayerLabel				= new Label("Inscrivez un nouveau joueur");
	static private Label existant					= new Label("");
	static private TextField addPlayerField			= new TextField();
	static private Button addPlayerButton			= new Button("Ajouter");
	final String regexAlpha	= "[A-Za-z-]*";
	final int nbMinPlayer							= 2;
	final int nbMaxPlayer							= 4;
	
	
	//shift option components
	static private Label shiftLabel					= new Label("Puissance [?]");
	final Spinner<Integer> shiftSpinner = new Spinner<Integer>();
	final int initialShiftValue = 4;
	final int minShiftValue = 4;
	final int maxShiftValue = 8;
	SpinnerValueFactory<Integer> shiftValueFactory = 	new SpinnerValueFactory.IntegerSpinnerValueFactory(minShiftValue, maxShiftValue, initialShiftValue);
	
	//grid size option components
	static private Label		labelSize			= new Label("Selectionnez la taille de la grille : ");
	static private Label		labelWidth			= new Label("Largeur : ");
	static private Label		labelHeight			= new Label("           Hauteur : ");
	static private TextField	widthOption			= new TextField("7");
	static private TextField	heightOption		= new TextField("6");
	final String regexNum	= "[0-9]*";
	
	//action button components
	static private Button	btnStart				= new Button("Commencer");
	static private Button	btnAccueil				= new Button("Accueil");
	
	public Configuration_v(){ 
		this.setController(null);
		this.setTitle("Puissance 4");
		this.setResizable(false);
		this.sizeToScene();
		Scene scene = new Scene(createContent());
		scene.getStylesheets().add("view/style.css");
	 	this.setScene(scene);
	}
	public Configuration_v(Conf_c controller) {
		this.setController(controller);
		this.setTitle("Puissance 4");
		//taille de fen�tre fixe (taille de la grille)
		this.setResizable(false);
		this.sizeToScene();
		Scene scene = new Scene(createContent());
		scene.getStylesheets().add("view/style.css");
	 	this.setScene(scene);
		
	}
	/**
	 * @return the controller
	 */
	public Conf_c getController() {
		return controller;
	}
	/**
	 * @param controller the controller to set
	 */
	public void setController(Conf_c controller) {
		this.controller = controller;
	}
	
	public static <T> boolean contains(final T[] array, final T v) {
	    for (final T e : array) {
	        if (e == v || v != null && v.equals(e)) {
	            return true;
	        }
	    }
	    return false;
	}
	public static boolean containsPlayer( Player_m[] array, String newPlayer) {
	    for (final Player_m e : array) {
	        if (e.getPseudo() == newPlayer || newPlayer != null && newPlayer.equals(e.getPseudo())) {
	            return true;
	        }
	    }
	    return false;
	}
	private ArrayList<String> setPlayerList(){
		ArrayList<String> playerList = new ArrayList<String>();
		for (final String e : playerView.getSelectionModel().getSelectedItems()) {
			playerList.add(e);
		}
		return playerList;
	}


	private VBox createPlayerListOption() {
		VBox vb = new VBox();
		ObservableList<String> playerList = this.controller.getPlayerList();
		playerView.setItems(playerList);
		playerView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		playerView.setOnMouseReleased(e->{
			if (playerView.getSelectionModel().getSelectedItems().size() > nbMaxPlayer) playerView.getStyleClass().add("btn-rouge");
			else playerView.getStyleClass().remove("btn-rouge");
		});
		playerView.setMaxHeight(150);
		
		HBox hb = new HBox();
		VBox addPlayerLabelField = new VBox();
		addPlayerField.setOnKeyReleased(e->{
			if (e.getCode()== KeyCode.ENTER) {
				if (addPlayerField.getText() != "" && addPlayerField.getText().matches(regexAlpha)) {
					int iPlayer = 0;
					boolean found = false;
					while (iPlayer<playerView.getItems().size() && !found) {
						if(playerView.getItems().get(iPlayer).equals(addPlayerField.getText()))
							found = true;
						else
							iPlayer++;
					}
					if (found) {
						playerView.getSelectionModel().select(iPlayer);
						existant.setText(addPlayerField.getText() + " existe deja");
					} else {
						playerList.add(addPlayerField.getText());
						Player_m.saveOneNewPlayer(addPlayerField.getText());
						playerView.setItems(playerList);
						playerView.getSelectionModel().select(playerView.getItems().size()-1);
					}
					addPlayerField.setText("");
				}
			}
		});
		addPlayerButton.setOnMouseClicked(e->{
			if (addPlayerField.getText()!="" && addPlayerField.getText().matches(regexAlpha)) {
				int iPlayer = 0;
				boolean found = false;
				while (iPlayer<playerView.getItems().size() && !found) {
					if(playerView.getItems().get(iPlayer).equals(addPlayerField.getText()))
						found = true;
					else
						iPlayer++;
				}
				if (found) {
					playerView.getSelectionModel().select(iPlayer);
					existant.setText(addPlayerField.getText() + " existe deja");
				} else {
					playerList.add(addPlayerField.getText());
					Player_m.saveOneNewPlayer(addPlayerField.getText());
					playerView.setItems(playerList);
					playerView.getSelectionModel().select(playerView.getItems().size()-1);
				}
				addPlayerField.setText("");
			}
			addPlayerField.requestFocus();
		});
		addPlayerLabelField.setSpacing(4);
		
		addPlayerLabelField.getChildren().addAll(addPlayerLabel,addPlayerField);
		hb.setSpacing(40);
		hb.getChildren().addAll(addPlayerLabelField,addPlayerButton,existant);
		
		
		vb.getChildren().addAll(playerView,hb);
		return vb;
	}
	
	private HBox createShiftOption() {
		HBox hb = new HBox();
		shiftSpinner.setValueFactory(shiftValueFactory);
		hb.setSpacing(10);
		hb.getChildren().addAll(shiftLabel,shiftSpinner);
		return hb;
	}
	
	private VBox createGridSizeOption() {
		VBox vb = new VBox();
		HBox sizeOptions = new HBox();
		
		
		widthOption.setOnKeyReleased(e-> {
			if (!widthOption.getText().matches(regexNum)) {
				widthOption.getStyleClass().add("text-field-error");
			}else {
				widthOption.getStyleClass().remove("text-field-error");
			}
		});
		heightOption.setOnKeyReleased(e-> {
			if (!heightOption.getText().matches(regexNum)) {
				heightOption.getStyleClass().add("text-field-error");
			}else {
				heightOption.getStyleClass().remove("text-field-error");
			}
		});
		sizeOptions.getChildren().addAll(labelWidth, widthOption, labelHeight, heightOption);
		vb.getChildren().addAll(labelSize,sizeOptions);
		return vb;
	}
	
	private HBox createOptionButtons() {
		HBox hb=new HBox();
		btnAccueil.setPrefWidth(200);
		btnAccueil.getStyleClass().add("btn-rouge");
		btnAccueil.setOnAction(e->{close();});
		
		btnStart.setPrefWidth(200);
		btnStart.getStyleClass().add("btn-vert");
		btnStart.setOnAction(e->{
			int nbSelectedPlayers = playerView.getSelectionModel().getSelectedItems().size();
			if(widthOption.getText().matches(regexNum) && heightOption.getText().matches(regexNum)) {
				if (nbSelectedPlayers >= nbMinPlayer && nbSelectedPlayers <= nbMaxPlayer) {
					if (Integer.parseInt(widthOption.getText()) >= shiftSpinner.getValue() && Integer.parseInt(heightOption.getText()) >= shiftSpinner.getValue()) {
						close();
						int nbLine=Integer.parseInt(heightOption.getText());
						int nbCol= Integer.parseInt(widthOption.getText());
						
						this.controller.setConfiguration(shiftSpinner.getValue(),nbLine, nbCol, setPlayerList());
						Main.openGame(this.controller.getModelGame());
					} else {
						widthOption.getStyleClass().add("btn-rouge");
						heightOption.getStyleClass().add("btn-rouge");
					}
				}
			}
		});
		hb.setAlignment(Pos.CENTER);
		hb.setSpacing(50);
		hb.getStyleClass().add("vb");
		hb.getChildren().addAll(btnAccueil,btnStart);
		return hb;
	}
	
	
	private VBox createContent() {
		VBox vb = new VBox();
		
		VBox playerListOption = createPlayerListOption();
		HBox shiftOption = createShiftOption();
		VBox gridSizeOption = createGridSizeOption();
		HBox optionButtons = createOptionButtons();
		
		vb.getChildren().addAll(playerListOption, shiftOption, gridSizeOption, optionButtons);
		return vb;
	}


}

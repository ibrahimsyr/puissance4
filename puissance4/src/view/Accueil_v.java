package view;

import general.Main;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Game_m;
import model.Player_m;
import observer.Observer;

public class Accueil_v extends Stage implements Observer {
	static private Button BTNLancer					= new Button("Lancer");	
	static private Button BTNAfficherScores					= new Button("Afficher les scores");
	static private Button BTNQuitter					= new Button("Quitter");
	
	public Accueil_v(){ 
		this.setTitle("Puissance 4");
		//taille de fenetre fixe (taille de la grille)
		this.setResizable(false);
		this.sizeToScene();
		Scene scene = new Scene(createContent(),500, 500);
		scene.getStylesheets().add("view/style.css");

		this.addListners();
	 	this.setScene(scene);
	}
	
	private VBox createContent() {
			VBox vb=new VBox();
			BTNAfficherScores.setMaxWidth(Double.MAX_VALUE);
			BTNAfficherScores.getStyleClass().add("btn-bleu");
			BTNLancer.setMaxWidth(Double.MAX_VALUE);
			BTNLancer.getStyleClass().add("btn-vert");
			BTNQuitter.setMaxWidth(Double.MAX_VALUE);
			BTNQuitter.getStyleClass().add("btn-rouge");
		     vb.getStyleClass().add("vb"); 
		      
		  //retrieving the observable list of the VBox 
	      ObservableList<Node> list = vb.getChildren(); 
	      
	      //Adding all the nodes to the observable list 
	      list.addAll(BTNLancer, BTNAfficherScores,BTNQuitter);       
	      
	      
	      
		return vb;
		
	}
	private void addListners() {
		BTNAfficherScores.setOnAction(e->{
			Player_m.initPlayerList();
			Main.openScores();
			});
		
		BTNLancer.setOnAction(e->{
			Player_m.initPlayerList();
			Main.openConfiguration();
			});
		
		BTNQuitter.setOnAction(e->{
			System.exit(0);
			});
	}
	@Override
	public void update(Game_m game) {
		// TODO Auto-generated method stub
		
	}

}

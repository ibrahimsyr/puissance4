package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Winner_v extends Stage {
	private Label winnerLabel = new Label("");
	private Button leave = new Button("Fermer");
	private Game_v view;
	
	public Winner_v() {
		this.setTitle("Nous avons un gagnant !");
		this.setResizable(false);
		this.sizeToScene();
		this.initModality(Modality.APPLICATION_MODAL);
		this.setOnCloseRequest(e->{close();view.close();});
		this.setScene(new Scene(creerContenu()));
	}
	
	public void setWinner(String winner) {
		winnerLabel.setText("Et le gagnant est ... " + winner);
	}
	
	public void setView(Game_v view) {
		this.view= view;
	}
	
	public VBox creerContenu() {
		VBox root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.setPrefWidth(200);
		root.setSpacing(40);
		VBox.setMargin(root, new Insets(20));
		leave.setOnAction(e->{close();view.close();});
		leave.getStyleClass().add("btn-rouge");
		root.getChildren().addAll(winnerLabel, leave);
		return root;
	}
}

package view;

import model.Player_m;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Scores_v extends Stage{
	private static Button buttonOk = new Button("Ok");
	static private TableView<Player_m> playerList 	= new TableView<Player_m>();
	
	public Scores_v(){ 
		this.setTitle("Puissance 4");
		this.setResizable(false);
		this.sizeToScene();
		Scene scene = new Scene(createContent());
		scene.getStylesheets().add("view/style.css");
	 	this.setScene(scene);
	}
	
	private ObservableList<Player_m> getPlayerList() {
		ObservableList<Player_m> playerList = FXCollections.observableArrayList();
		for (Player_m e : Player_m.playerList) playerList.add(e);
		return playerList;
	}
	
	@SuppressWarnings("unchecked")
	private TableView<Player_m> createPlayerList() {
		TableView<Player_m> players = new TableView<Player_m>();
		TableColumn<Player_m, String> playerPseudoColumn	= new TableColumn<Player_m,String>("Pseudo");
		TableColumn<Player_m, Integer> playerScoreColumn	= new TableColumn<Player_m,Integer>("Score");
		players.getColumns().addAll(playerPseudoColumn, playerScoreColumn);
		players.setItems(getPlayerList());
		playerPseudoColumn.setCellValueFactory(new PropertyValueFactory<>("pseudo"));
		playerScoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
		playerScoreColumn.setSortType(TableColumn.SortType.DESCENDING);
		playerPseudoColumn.setSortType(TableColumn.SortType.ASCENDING);
		players.getSortOrder().addAll(playerScoreColumn,playerPseudoColumn);
		players.sort();
		return players;
	}
	
	private VBox createContent() {
		playerList = createPlayerList();
		buttonOk.getStyleClass().add("btn-bleu");
		buttonOk.setOnAction(e->{
			this.close();
		});
		return new VBox(playerList, buttonOk);
	}
}

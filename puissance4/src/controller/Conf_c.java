package controller;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Game_m;
import model.Player_m;

public class Conf_c {
	
	private  Game_m modelGame;

	public Conf_c(Game_m myGame) {
		modelGame =myGame;
	}

	public Game_m getModelGame() {
		return modelGame;
	}

	public void setModelGame(Game_m modelGame) {
		this.modelGame = modelGame;
	}
	
	
	public void setConfiguration(int shift, int nbLine, int nbCol,ArrayList<String> playerListPseudo) {
		/**
		 * @brief preparer la lsite de player afin de la passer au model
		 *  
		 * @return list player
		 */
		ArrayList<Player_m> playerListObj=new ArrayList<Player_m> ();
		for(int i=0;i<playerListPseudo.size();i++) {
			playerListObj.add(new Player_m(playerListPseudo.get(i)));
		}
		this.modelGame.setShift(shift);
		this.modelGame.setnbCol(nbCol);
		this.modelGame.setNbLine(nbLine);
		this.modelGame.setPlayerList(playerListObj);
		this.modelGame.initGridCase(nbLine, nbCol);
		this.modelGame.initHeight(nbCol);
	}
	public ObservableList<String> getPlayerList() {
		ObservableList<String> playerList = FXCollections.observableArrayList();
		for (Player_m e : Player_m.playerList) playerList.add(e.getPseudo());
		
		return playerList;
	}
}

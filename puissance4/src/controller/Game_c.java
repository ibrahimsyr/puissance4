package controller;

import model.Game_m;

public  class Game_c {
	private  Game_m modelGame;


	public Game_c(Game_m myGame) {
		this.modelGame =myGame;
	}
	public Game_m getModelGame() {
		return modelGame;
	}

	public void setModelGame(Game_m modelGame) {
		this.modelGame = modelGame;
	}

	

	
	
	/**
	 * 
	 * @param column
	 * @return boolean :
	 * 					- 1 if the user can't play
	 * 					- 0 if the token has been dropped
	 */
	public  boolean drop(int column) {
		boolean res = true;
		if(this.modelGame.canPlay(column)) {
			this.modelGame.play(column);
			res=false;
		}
		return res;
	}
}

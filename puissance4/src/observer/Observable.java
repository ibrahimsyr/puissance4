package observer;

import model.Game_m;

public interface Observable {
	  public void addObserver(Observer obs);
	  public void removeObserver();
	  public void notifyObserver(Game_m game);
	
}

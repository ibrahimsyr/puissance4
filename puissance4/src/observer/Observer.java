package observer;

import model.Game_m;

public interface Observer {
  public void update(Game_m game);
}
package state;

import java.io.Serializable;

import model.Player_m;

public class StopState implements State, Serializable {

	private static final long serialVersionUID = -8155427578847568658L;

	public void doAction(Player_m p) {
	      p.setState(this);	
	   }

	   public String toString(){
	      return "Stop State";
	   }
	}
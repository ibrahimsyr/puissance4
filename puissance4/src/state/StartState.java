package state;

import model.Player_m;

public class StartState implements State {

	   public void doAction(Player_m p) {
	      p.setState(this);	
	   }

	   public String toString(){
	      return "Start State";
	   }
	}
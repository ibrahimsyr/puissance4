package general;

import java.util.ArrayList;

import controller.Conf_c;
import controller.Game_c;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Game_m;
import model.Player_m;
import model.Sauvegarde;
import view.Accueil_v;
import view.Configuration_v;
import view.Game_v;
import view.Winner_v;
import view.Scores_v;

public class Main extends Application {

	static private Winner_v winnerModal;
	static private Accueil_v accueil;
	static private Scores_v scores;
	static private Configuration_v conf;
	static private Game_v gameView;
	
	public void start(Stage myStage) {
		accueil = new Accueil_v();
		accueil.show();
	}
	
	public static void main(String[] args) {
		/* Réinitialisation de la liste de joueurs*/
		Sauvegarde.write(new ArrayList<Player_m>(), Player_m.ficName);
		launch(args);
	}
	
	public static void openConfiguration() {
		conf = new Configuration_v(new Conf_c(new Game_m()));
		conf.show();
	}
	
	public static void openGame(Game_m gameModel) {
		gameModel.start();
		Game_c gameControler	= new Game_c(gameModel);
		gameView				= new Game_v(gameControler);
		gameModel.addObserver(gameView);
		gameView.show();
	}
	
	public static void openWinnerModal(Game_v view, Player_m winner) {
		winnerModal = new Winner_v();
		winnerModal.setWinner(winner.getPseudo());
		winnerModal.setView(view);
		winnerModal.show();
	}
	
	public static void openScores() {
		scores = new Scores_v();
		scores.show();
	}
}
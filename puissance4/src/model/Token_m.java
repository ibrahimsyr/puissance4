package model;

public class Token_m  implements Comparable<Token_m>{
	//Token identifier
	 private static int nb=0;
	 private int id;
	//identifiant de joueur de ce Token
	private Player_m player;
	//sa postion dans le tableau
	private Position position;

	
	
	public Token_m() {
		position=new Position();
		Token_m.nb++;
		this.setId();
		this.setPlayer(null);

	}


	public Token_m( Player_m player,int x,int y) {
		position=new Position(x,y);
		Token_m.nb++;
		this.setId();
		this.setPlayer(player);
	}
	public int getId() {
		return id;
	}
	public void setId() {
		this.id = Token_m.nb;
	}
	public Player_m getPlayer() {
		return player;
	}
	public void setPlayer(Player_m player) {
		this.player = player;
	}
	public String toString() {
		return "" + this.id + " : " + this.player;
	}
	public Position getPosition() {
		return this.position;
	}
	public void setPosition(int line,int col) {
		this.position.setLine(line);
		this.position.setCol(col);
	}


	@Override
	public int compareTo(Token_m token) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				 if (this.getPlayer().getPseudo()==token.getPlayer().getPseudo()) {
					 return 0;
				 }else {
					 return 1;
				 }
	}

	
}

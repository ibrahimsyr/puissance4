package model;

import java.util.ArrayList;

import observer.Observable;
import observer.Observer;
import view.Game_v;

public class Game_Abstract implements Observable {
	
	  private ArrayList<Observer> listObserver = new ArrayList<Observer>();   
	  //Implémentation du pattern observer
	  public void addObserver(Game_v myGameWind) {
	    this.listObserver.add(myGameWind);
	  }

	  public void notifyObserver(Game_m game) {
	   

	    for(Observer obs : listObserver)
	      obs.update(game);
	  }

	  public void removeObserver() {
	    listObserver = new ArrayList<Observer>();
	  }

	@Override
	public void addObserver(Observer obs) {
		// TODO Auto-generated method stub
		
	}  
}

package model;
import java.util.ArrayList;
import java.util.Random;

import state.StartState;
import state.StopState;

public class Game_m extends Game_Abstract {
	private ArrayList<Player_m> playerList; //Players who play this game
	private int nbLine;
	private int nbCol;
	private Token_m[][] grid; //Game Grid represented by a Table
	private int[] height;
	private int shift = 4;
	
	private boolean win;
	private Player_m winner;
	private Token_m LastToken;
	private int nbTokens=0;
	private int nbTours=0;
	public boolean getWin() {
		return this.win;
	}
	public Game_m(int nbLine, int nbCol) {
		this.win=false;
		this.winner=null;
		this.setPlayerList(new ArrayList<Player_m>());
		this.setnbCol(nbCol);
		this.setNbLine(nbLine);
	}

	public Game_m(int nbLine, int nbCol, Player_m player1, Player_m player2) {
		this.setPlayerList(new ArrayList<Player_m>());
		this.setnbCol(nbCol);
		this.setNbLine(nbLine);
		this.win=false;
		this.winner=null;
		this.getPlayerList().add( player1);
		this.getPlayerList().add( player2);
		this.initGridCase(nbLine, nbCol);
		this.initHeight(nbLine);
	}
	public Game_m(int nbLine, int nbCol,ArrayList<Player_m> playerList) {
		this.setPlayerList(playerList);
		this.setnbCol(nbCol);
		this.setNbLine(nbLine);
		this.initGridCase( nbLine, nbCol);
		this.initHeight(nbLine);
	}

	public Game_m() {
		this.win=false;
		this.winner=null;
		this.setPlayerList(new ArrayList<Player_m>());
		this.setnbCol(0);
		this.setNbLine(0);
		this.initGridCase(nbLine, nbCol);
		this.initHeight(nbLine);
	}

	public void initHeight(int nbLine) {
		this.height=new int[nbLine];
		for (int i = 0;i < nbLine ; i++) {
			this.height[i]=0;
		}
	}
	public void initGridCase(int nbLine,int nbCol) {
		this.grid=new Token_m[nbLine][nbCol];
		for (int i = 0;i < nbLine ; i++) {
			for (int j = 0;j < nbCol; j++) {
				Token_m t=new Token_m();

				this.grid[i][j]=t;
				//this.setGridCase(j, i, new Token_m());
			}
		}
	}

	public int getShift() {
		return shift;
	}
	public void setShift(int shift) {
		this.shift = shift;
	}
	public int getNbLine() {
		return nbLine;
	}

	public void setNbLine(int nbLine) {
		this.nbLine = nbLine;
	}

	public Token_m[][] getGrid() {
		return grid;
	}

	public void setGrid(Token_m[][] grid) {
		this.grid = grid;
	}

	public Token_m getGridCase(int row, int column) {
		return this.grid[row][column];
	}

	public void setGridCase(int row, int column, Token_m token) {
		this.grid[row][column] = token;
	}

	/**
	 * @return the playerList
	 */
	public ArrayList<Player_m> getPlayerList() {
		return playerList;
	}

	/**
	 * @param playerList the playerList to set
	 */
	public void setPlayerList(ArrayList<Player_m> playerList) {
		this.playerList = playerList;
	}
	/***
	 * 
	 * add a new player to list players
	 */
	public void addPlayer(Player_m p) {
		this.playerList.add(p);
	}


	/**
	 * @return the nbCol
	 */
	public int getNbCol() {
		return nbCol;
	}

	/**
	 * @param nbCol the nbCol to set
	 */
	public void setnbCol(int nbCol) {
		this.nbCol = nbCol;
	}
	/***
	 * 
	 * 
	 */
	public String toString() {
		return  " the winner is  :: "+this.winner;
	}
	public void setWinner(Player_m p) {
		this.winner=p;
	}
	public Player_m getWinner() {
		return this.winner;
	}

	/**
	 * 
	 * 
	 */
	public void start() {
		Random rn = new Random();
		int firstplayer = rn.nextInt(playerList.size());
		playerList.get(firstplayer).setState(new StartState()); 

		this.notifyObserver(this);
	}
	/**
	 * 
	 * 
	 * @param col
	 */
	public boolean canPlay(int col) {
		return (this.height[col]<this.getNbLine());
	}
	/**
	 * 
	 * 
	 */
	public void play(int col) {

		this.setNbTokens(this.getNbTokens()+1);
		this.setNbTours(this.nbTokens/this.getPlayerList().size()+1);


		this.LastToken=this.getGridCase(this.nbLine-this.height[col]-1,col);
		LastToken.setPosition(this.nbLine-this.height[col]-1,col);
		LastToken.setPlayer(getCurrentPlayer());//lowest line in the column 
		if(this.win(LastToken)) {
			this.setWinner(this.getCurrentPlayer());
			Player_m.addScoreToOnePlayer(this.getWinner());
		} 


		this.height[col]++;//add one to the height of column

		this.notifyObserver(this);
		this.nextPlayer();// passe to the next player


	}
	/**
	 * 
	 * 
	 */
	public void nextPlayer() {
		int i=0;
		while(!(this.playerList.get(i).getState() instanceof StartState) && i<this.playerList.size()) {
			i++;
		}
		if(this.playerList.get(i).getState() instanceof StartState)//si le ce playerpeut jouer et c'est son tour
		{
			this.playerList.get(i).setState(new StopState());//desactive the current player
			this.playerList.get((i+1)%this.playerList.size()).setState(new StartState());///active nexte one 
		}
	}

	/**
	 * 
	 * current player
	 */
	public Player_m getCurrentPlayer() {
		Player_m p=null;
		for(int i = 0 ; i < this.playerList.size(); i++) {
			if(this.playerList.get(i).getState() instanceof StartState) {//si le ce player  peut jouer et c'est son tour
				p=this.playerList.get(i);

			}

		}	 
		return p;
	}
	/***
	 * 
	 * 
	 */
	public boolean win(Token_m token) {
		this.win=this.verifyDiagonale1(token)||this.verifyDiagonale2(token)||this.verifyHorizintal(token)||this.verifyVertical(token);
		return this.win;
	}
	/***
	 * 
	 * @param token
	 * @return boolean 
	 * @bref verify if there is enough token in same column
	 */
	public boolean verifyVertical(Token_m token) {
		int somme = 1;
		
		int line	= token.getPosition().getLine() + 1;
		int col		= token.getPosition().getCol();

		while(line < this.getNbLine() && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			line++;
			
			somme++;
		}
		return somme == this.getShift();
	}
	/***
	 *  @see a verifier
	 * @param token
	 * @return boolean 
	 * @bref verify if there is enough token in same line
	 */
	public boolean verifyHorizintal(Token_m token) {
		int somme	= 1;
		
		int line	= token.getPosition().getLine(); 
		int col		= token.getPosition().getCol() - 1;

		while(col >= 0 && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			col--;
			
			somme++;
		}
		
		col			= token.getPosition().getCol() + 1;
			
		while(col < this.getNbCol() && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			col++; 
			
			somme++;
		}

		return somme == this.getShift();

	}
	/***
	 * @see a verifier
	 * @param token
	 * @return boolean 
	 * @bref verify if there is enough token in same diagonal (increasing)
	 */
	public boolean verifyDiagonale1(Token_m token) {
		int somme	= 1;
		int line	= token.getPosition().getLine() - 1; 
		int col		= token.getPosition().getCol() + 1;
		while(line >= 0 && col < this.getNbCol() && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			line--;
			col++;
			somme++;
		}
		line		= token.getPosition().getLine() + 1;
		col			= token.getPosition().getCol() - 1;
		while(line < this.getNbLine() && col >= 0 && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			line++; 
			col--;
			somme++;
		}
		return somme == this.getShift();
	}
	
	/***
	 * 
	 * @param token
	 * @return boolean 
	 * @bref verify if there is enough token in same diagonal (decreasing)
	 */
	public boolean verifyDiagonale2(Token_m token) {
		int somme	= 1;
		
		int line	= token.getPosition().getLine() - 1; 
		int col		= token.getPosition().getCol() - 1;
		
		while(line >= 0 && col >= 0 && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			line--;
			col--;
			
			somme++;
		}
		
		line		= token.getPosition().getLine() + 1;
		col			= token.getPosition().getCol() + 1;
		
		while(line < this.getNbLine() && col < this.getNbCol() && this.getGridCase(line,col).getPlayer() != null && this.getGridCase(line,col).getPlayer().compareTo(token.getPlayer()) == 0) {
			line++;
			col++;
			
			somme++;
		}

		return somme == this.getShift();
	}
	/**
	 * @return the lastToken
	 */
	public Token_m getLastToken() {
		return LastToken;
	}
	/**
	 * @param lastToken the lastToken to set
	 */
	public void setLastToken(Token_m lastToken) {
		LastToken = lastToken;
	}
	/**
	 * @return the nbTokens
	 */
	public int getNbTokens() {
		return nbTokens;
	}
	/**
	 * @param nbTokens the nbTokens to set
	 */
	public void setNbTokens(int nbTokens) {
		this.nbTokens = nbTokens;
	}
	/**
	 * @return the nbTours
	 */
	public int getNbTours() {
		return nbTours;
	}
	/**
	 * @param nbTours the nbTours to set
	 */
	public void setNbTours(int nbTours) {
		this.nbTours = nbTours;
	}


}

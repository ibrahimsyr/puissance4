package model;

import java.io.Serializable;
import java.util.ArrayList;

import state.*;

//variable globale
public class Player_m implements Comparable<Player_m>, Serializable {	
	private static final long serialVersionUID = 1L;
	public  static final String ficName = "dbtest";
	
	public static ArrayList<Player_m> playerList = new ArrayList<Player_m>();
	
	
	private String pseudo;
	private int score;
	private State state;
	
	
	
	public Player_m(String pseudo, int score) {
		this.setPseudo(pseudo);
		this.setScore(score);
		
	}
	
	public Player_m(String pseudo) {
		this.setPseudo(pseudo);
		this.setScore(0);
		this.setState(new StopState());
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public String getPseudo() {
		return this.pseudo;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public String toString() {
		return this.pseudo + " (" + this.score + ")";
	}
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}


	@Override
	public int compareTo(Player_m player) {
		int res = 1;
		if (this.getPseudo().compareTo(player.getPseudo())==0){
			res = 0;
		}
		return res;
	}

	public Boolean existsPlayer(ArrayList<Player_m> playerList) {
		int i = 0;
		Boolean res = false;
		while (i < playerList.size()) {
			if (this.compareTo(playerList.get(i)) == 0) {
				res = true;
			}
			i++;
		}
		return res;
	}
	public static void saveOneNewPlayer(String playerName) {
		Player_m newPlayer = new Player_m(playerName);
			if(!newPlayer.existsPlayer(playerList)) {
				playerList.add(newPlayer);
				Sauvegarde.write(playerList, ficName);
			}
	}
	public static void addScoreToOnePlayer(Player_m player) {
		int i = 0;
		while (i < playerList.size()) {
			if (player.compareTo(playerList.get(i)) == 0) {
				playerList.get(i).setScore(playerList.get(i).getScore()+1);
			}
			i++;
		}
		Sauvegarde.write(playerList, ficName);
	}
	public static void initPlayerList() {
			playerList = Sauvegarde.read(ficName);
	}
}

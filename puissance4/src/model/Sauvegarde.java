package model;



import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Sauvegarde implements Serializable{
    private static final long serialVersionUID = 1L;
    
    public static void write(ArrayList<Player_m> f, String nomFic) {
        ObjectOutputStream sortie;
		try {
			sortie = new ObjectOutputStream(new FileOutputStream(nomFic,false));
	        sortie.writeObject(f);
	        sortie.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
	@SuppressWarnings("unchecked")
	public static ArrayList<Player_m> read(String nomFic){
        ObjectInputStream entree = null;
        ArrayList<Player_m> playerList = new ArrayList<Player_m>();
		try {
			entree = new ObjectInputStream(new FileInputStream(nomFic));
			while(true) {
				playerList=(ArrayList<Player_m>)entree.readObject();
			}
		} catch (EOFException e) {
			try {
				entree.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} 
        return playerList;
    }
}
package model;

public class Position {
	private int line;
	private int col;
	public Position() {
		this.setLine(0);
		this.setCol(0);
	}
public Position(int line,int col) {
	this.setLine(line);
	this.setCol(col);
}

	/**
	 * @param y the y to set
	 */
	public void setlineCol(int line,int col) {
		this.setLine(col);
		this.setCol(line);
	}
	/**
	 * 
	 * 
	 * @return
	 */
	public Position getlineCol() {
		return this;
	}
	/**
	 * @return the line
	 */
	public int getLine() {
		return line;
	}
	/**
	 * @param line the line to set
	 */
	public void setLine(int line) {
		this.line = line;
	}
	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}
	/**
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}
	/**
	 * 
	 */
	public String toString() {
		return this.getLine()+" col "+this.getCol();
	}
}
